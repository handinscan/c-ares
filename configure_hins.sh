#!/bin/bash
set -x
CONFIGURE_TOOLCHAIN_PARAM=

if [ ! -z ${COMPILER} ]; then
	CONFIGURE_TOOLCHAIN_PARAM="--target=${TOOLCHAIN_TOOL_NAME_PREFIX} --host=${TOOLCHAIN_TOOL_NAME_PREFIX} "
	
	BUILDROOT_VERSION=$(jq -c ".dependencies[] | select ( .artifact_name | contains(\"buildroot\")) | .version" ./version.json | xargs)
	BUILDROOT_STAGING=${ARTIFACT_REPO_ROOT_DIR}/arm_gcc9/buildroot/${BUILDROOT_VERSION}/host/usr/arm-buildroot-linux-gnueabihf/sysroot/
	
	SSL_CONFIG=\--with-ssl=${BUILDROOT_STAGING}/usr/\ \--with-ca-path=/etc/ssl/certs

	export LDFLAGS=-L${BUILDROOT_STAGING}/usr/lib/ ${LDFLAGS}
	export LDFLAGS=-L${BUILDROOT_STAGING} ${LDFLAGS}
	export CPPFLAGS=-I${BUILDROOT_STAGING}/usr/include/ ${CPPFLAGS}
fi
export LIBTOOLFLAGS=-static

./configure --prefix=${RESULT_DIR}\
			--exec-prefix=${RESULT_DIR}\
			${SSL_CONFIG}\
			--disable-shared  --enable-static --disable-tests  \
			${CONFIGURE_TOOLCHAIN_PARAM}
