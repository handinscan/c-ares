#!/bin/bash

ARTIFACT_REPO_ROOT_DIR=$1
TOOLCHAIN=$2

VERSION_JSON=./version.json
TOOL_CMAKE_VERSION=$(jq ".build_tool.version" $VERSION_JSON | tr -d '"')
TOOL_CMAKE_ROOT=${ARTIFACT_REPO_ROOT_DIR}/host/tool_cmake/${TOOL_CMAKE_VERSION}
ARTIFACT_REPO_STORE_SH=${TOOL_CMAKE_ROOT}/artifact_repo/store.sh

function error {
	echo "build error"
	exit 1
}

function usage {
	echo "compile.sh /artifact_repo/absolute/path/ toolchain_name|default:latest"
	echo "\$1 - Artifact repo path (mand)"
	echo "\$2 - Toolchain name or alias. Default is latest. (opt)"
}

if [ -z ${ARTIFACT_REPO_ROOT_DIR} ]; then
	echo "Error: Artifact repo is not set."
	usage
	error
fi

if [ -z ${TOOLCHAIN} ]; then
	TOOLCHAIN=latest
	echo "Toolchain is set to \"latest\""
fi

echo "remove old build script(s)"
rm ./build_script_*

echo "generate new build script(s)"
${TOOL_CMAKE_ROOT}/version/gen_autotools_build_script.sh \
	${VERSION_JSON} ${TOOLCHAIN}	\
	${ARTIFACT_REPO_ROOT_DIR}		\
	${ARTIFACT_REPO_STORE_SH} || error

echo "execute the build scripts"
for file in $(ls build_script_*) ; do
	echo "execute: $file"
	./${file} || error
done